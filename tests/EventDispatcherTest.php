<?php

namespace Keepper\Lib\Events\Tests;

use Keepper\Lib\Events\EventDispatcher;
use Keepper\Lib\Events\Interfaces\EventDispatcherInterface;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class EventDispatcherTest extends \PHPUnit_Framework_TestCase {
	private $countFirst = 0;
	private $countSecond = 0;

	/**
	 * @var EventDispatcherInterface
	 */
	protected $dispatcher;

	public function setUp() {
		$logger = new Logger('UnitTests');
		$logger->pushHandler(new StreamHandler('php://stderr', Logger::DEBUG));
		$this->dispatcher = new EventDispatcher(null, $logger);
	}

	public function testDispatchWithoutListeners() {
		$this->dispatcher->dispatch('test-event-name');
	}

	public function simpleListenerFirst() {
		$this->countFirst += 1;
	}

	public function simpleListenerSecond() {
		$this->countSecond += 1;
	}

	public function testDispathWithoutArguments() {
		$this->countFirst = 0;
		$this->countSecond = 0;

		// Добавляем несколько слушателей на одно событие
		$this->dispatcher->subscriber()->addListener('test-event-first', [$this, 'simpleListenerFirst']);
		$this->dispatcher->subscriber()->addListener('test-event-first', [$this, 'simpleListenerFirst']);
		$this->dispatcher->subscriber()->addListener('test-event-first', [$this, 'simpleListenerFirst']);

		// И слушателя на другое событие
		$this->dispatcher->subscriber()->addListener('test-event-second', [$this, 'simpleListenerSecond']);

		$this->dispatcher->dispatch('test-event-first');

		$this->assertEquals(3, $this->countFirst, 'Ожидали вызова трех подписчиков');
		$this->assertEquals(0, $this->countSecond, 'Не ожидали вызова подписчика подписанного на другое событие');
	}

	public function listenerWithArguments($a = null, $b = null, $c = null) {
		$this->countFirst += 1;
		$this->assertEquals('test', $a, 'Ожидали иное значение в первом передаваемом подписчику аргументе');
		$this->assertEquals(1, $b, 'Ожидали иное значение во втором передаваемом подписчику аргументе');
		$this->assertTrue($c, 'Ожидали иное значение в третьем передаваемом подписчику аргументе');
	}

	public function testDispatchWithArguments() {
		$this->countFirst = 0;

		// Добавляем несколько слушателей на одно событие
		$this->dispatcher->subscriber()->addListener('test-event-first', [$this, 'listenerWithArguments']);

		$this->dispatcher->dispatch('test-event-first', ['test', 1, true]);

		$this->assertEquals(1, $this->countFirst, 'Ожидали вызова подписчика');
	}
}