<?php
namespace Keepper\Lib\Events\Tests;

use Keepper\Lib\Events\EventSubscriber;
use Keepper\Lib\Events\EventSubscriberProxy;

class EventSubscriberProxyTest extends \PHPUnit_Framework_TestCase {

	/**
	 * @var EventSubscriberProxy
	 */
	protected $proxy = null;

	public function setUp() {
		parent::setUp();

		$this->proxy = new class(new EventSubscriber()) extends EventSubscriberProxy {};
	}

	public function testHasListeners() {
		$handler = function(){};
		$this->assertFalse($this->proxy->hasListeners('test-event'), 'Изначально не ожидаем наличие подписчиков');

		$this->proxy->addListener('test-event', $handler);

		$this->assertTrue($this->proxy->hasListeners('test-event'), 'Ожидали наличие подписчиков после подписки на событие');

		return $handler;
	}

	/**
	 * @depends testHasListeners
	 */
	public function testRemoveListener($handler) {
		$this->assertFalse($this->proxy->hasListeners('test-event'), 'Ожидаем подписчика, от зависимого теста');

		$this->proxy->removeListener('test-event', $handler);

		$this->assertFalse($this->proxy->hasListeners('test-event'), 'Не ожидаем подписчика, после отписки');
	}
}