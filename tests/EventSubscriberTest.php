<?php

namespace Keepper\Lib\Events\Tests;

use Keepper\Lib\Events\EventSubscriber;
use Keepper\Lib\Events\Interfaces\EventSubscriberInterface;

class EventSubscriberTest extends \PHPUnit_Framework_TestCase {

	/**
	 * @var EventSubscriberInterface
	 */
	protected $subscriber;

	public function setUp() {
		parent::setUp();
		$this->subscriber = new EventSubscriber();
	}

	/**
	 * @dataProvider listenerDataProvider
	 * @param $listener
	 */
	public function testListeners($listener) {
		$eventName = 'test-event-name';
		$this->assertFalse($this->subscriber->hasListeners($eventName), 'Изначально не должно быть подписчиков');

		// Добавляем слушатель как анонимную функцию
		$this->subscriber->addListener($eventName, $listener);

		// Проверяем что теперь есть подписчики на данное событие
		$this->assertTrue($this->subscriber->hasListeners($eventName), 'После добавления слушателя ожидали, что будут подписчики');

		$listeners = $this->subscriber->getListeners($eventName);
		$this->assertEquals($eventName, $listeners->eventName(), 'Ожидали что колекцию слушателей именно на запрошеное событие');

		try {
			foreach ($listeners as $listener) {
				// Запускаем слушателей
				$this->runListener($listener);
			}
			$this->assertFalse(true, 'Из ожидаемого слушателя должно вылететь исключение');
		} catch (\Exception $e) {
			$this->assertEquals('Listener has run', $e->getMessage(), 'Не должно быть исключений нами не ожидаемых');
		}

		// Удаляем слушателя
		$this->subscriber->removeListener($eventName, $listener);
		$this->assertFalse($this->subscriber->hasListeners($eventName), 'Не должно остаться подписчиков после удаления слушателя');
	}

	protected function runListener(callable $listener) {
		$listener();
	}

	public function listenerDataProvider() {
		return [
			[function() {	throw new \Exception('Listener has run'); }],
			[[$this, 'listener']],
			[[self::class, 'staticListener']]
		];
	}

	public function listener() {
		throw new \Exception('Listener has run');
	}

	public static function staticListener() {
		throw new \Exception('Listener has run');
	}

	public function testRemoveListener() {
		$eventName = 'test-event-name';
		$this->assertFalse($this->subscriber->hasListeners($eventName), 'Изначально не должно быть подписчиков');

		$handler1 = function(){};
		$called = false;
		$handler2 = function() use(&$called){
			$called = true;
		};

		$this->subscriber->addListener($eventName, $handler1);
		$this->subscriber->addListener($eventName, $handler2);

		$this->assertTrue($this->subscriber->hasListeners($eventName), 'Должны быть подписчики');
		$this->assertCount(2, $this->subscriber->getListeners($eventName), 'Ожидаем 2х подписчиков');

		// Удаляем одного из подписчиков
		$this->subscriber->removeListener($eventName, $handler1);

		$this->assertTrue($this->subscriber->hasListeners($eventName), 'Должны быть подписчики');
		$listenerCollection = $this->subscriber->getListeners($eventName);
		$this->assertCount(1, $listenerCollection, 'Ожидаем одного подписчика после отписки');
		$this->assertEquals(0, $listenerCollection->key(), 'Ожидаем, что внутрення позиция коллекции стоит в начале');

		// Запускаем оставшийся подписчик
		$this->runListener($this->subscriber->getListeners($eventName)->current());

		$this->assertTrue($called, 'Ожидаем что исполнится второй подписчик');
	}
}