<?php

namespace Keepper\Lib\Events;

use Keepper\Lib\Events\Interfaces\EventListenerCollectionInterface;

class EventListenerCollection implements EventListenerCollectionInterface {
	private $position = 0;
	private $eventName = null;
	private $listeners = [];

	public function __construct(string $eventName) {
		$this->eventName = $eventName;
		$this->position = 0;
	}

	public function attach(callable $listener) {
		$this->listeners[] = $listener;
	}

	public function current(): callable {
		return $this->listeners[$this->position];
	}

	public function rewind() {
		$this->position = 0;
	}

	public function count(): int {
		return count($this->listeners);
	}

	public function key() {
		return $this->position;
	}

	public function next() {
		++$this->position;
	}

	public function valid(){
		return isset($this->listeners[$this->position]);
	}

	public function eventName(): string {
		return $this->eventName;
	}

	public function remove(callable $listener) {
		$listeners = [];
		foreach ($this->listeners as $existsListener) {
			if ($existsListener == $listener) {
				break;
			}
			$listeners[] = $existsListener;
		}
		$this->position = 0;
		$this->listeners = $listeners;
	}
}