<?php

namespace Keepper\Lib\Events;

use Keepper\Lib\Events\Interfaces\EventListenerCollectionInterface;
use Keepper\Lib\Events\Interfaces\EventSubscriberInterface;

abstract class EventSubscriberProxy implements EventSubscriberInterface {
	/**
	 * @var EventSubscriberInterface
	 */
	protected $subscriber = null;

	public function __construct(EventSubscriberInterface $eventSubscriber) {
		$this->subscriber = $eventSubscriber;
	}

	public function addListener(string $eventName, callable $listener): void {
		$this->subscriber->addListener($eventName, $listener);
	}

	public function removeListener(string $eventName, callable $listener): void {
		$this->subscriber->removeListener($eventName, $listener);
	}

	public function getListeners(string $eventName): EventListenerCollectionInterface {
		return $this->subscriber->getListeners($eventName);
	}

	public function hasListeners(string $eventName): bool {
		return $this->subscriber->hasListeners($eventName);
	}
}