<?php

namespace Keepper\Lib\Events;

use Keepper\Lib\Events\Interfaces\EventDispatcherInterface;
use Keepper\Lib\Events\Interfaces\EventSubscriberInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

class EventDispatcher implements EventDispatcherInterface {
	const BEFORE_DISPATCH = 'beforeDispathEvent';
	use LoggerAwareTrait;

	private $subscriber = null;

	public function __construct(
		EventSubscriberInterface $subscriber = null,
		LoggerInterface $logger = null
	) {
		if ( is_null($subscriber) ) {
			$subscriber = new EventSubscriber();
		}
		$this->subscriber = $subscriber;

		$this->setLogger($logger ?? new NullLogger());
	}

	/**
	 * @inheritdoc
	 */
	public function dispatch(string $eventName, array $arguments = []): void {
		if ( $eventName != self::BEFORE_DISPATCH ) {
			$this->logger->debug('EventManager: start dispathing event "'.$eventName.'"');
			$this->dispatch(self::BEFORE_DISPATCH, [$eventName]);
		}

		$called = 0;
		$success = 0;
		$errors = 0;

		$listeners = $this->subscriber()->getListeners($eventName);
		foreach ($listeners as $listener) {
			try {
				$this->callListener($listener, $arguments);
				$called++;
				$success++;
			} catch (\Exception $e) {
				$errors++;
				$this->logger->error('EventManager: Error on Event "'.$eventName.'" handler. '.$e->getMessage()."\n".$e->getTraceAsString());
			}
		}
		if ( $eventName != self::BEFORE_DISPATCH ) {
			$this->logger->debug('EventManager: finished dispathing event "' . $eventName . '" called: ' . $called . '/' . count($listeners) . ', success: ' . $success . ', errors: ' . $errors);
		}
	}

	protected function callListener(callable $listener, array $arguments) {
		call_user_func_array($listener, $arguments);
	}

	public function subscriber(): EventSubscriberInterface {
		return $this->subscriber;
	}
}