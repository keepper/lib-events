<?php

namespace Keepper\Lib\Events;

use Keepper\Lib\Events\Interfaces\EventListenerCollectionInterface;
use Keepper\Lib\Events\Interfaces\EventSubscriberInterface;

class EventSubscriber implements EventSubscriberInterface {
	private $events = [];

	/**
	 * @inheritdoc
	 */
	public function addListener(string $eventName, callable $listener): void {
		$listenerCollection = $this->getCollectionByEventName($eventName);
		$listenerCollection->attach($listener);
		$this->events[$eventName] = $listenerCollection;
	}

	/**
	 * @inheritdoc
	 */
	public function removeListener(string $eventName, callable $listener): void {
		$listenerCollection = $this->getCollectionByEventName($eventName);
		$newListenerCollection = new EventListenerCollection($eventName);
		foreach ($listenerCollection as $existsListener) {

			if ($existsListener == $listener) {
				continue;
			}
			$newListenerCollection->attach($existsListener);
		}
		$this->events[$eventName] = $newListenerCollection;
	}

	/**
	 * @inheritdoc
	 */
	public function getListeners(string $eventName): EventListenerCollectionInterface {
		return $this->getCollectionByEventName($eventName);
	}

	/**
	 * @inheritdoc
	 */
	public function hasListeners(string $eventName): bool {
		if ( !array_key_exists($eventName, $this->events) ) {
			return false;
		}

		return 0 != $this->getCollectionByEventName($eventName)->count();
	}

	private function getCollectionByEventName(string $eventName): EventListenerCollectionInterface {
		if ( !array_key_exists($eventName, $this->events) ) {
			return new EventListenerCollection($eventName);
		}

		return $this->events[$eventName];
	}
}