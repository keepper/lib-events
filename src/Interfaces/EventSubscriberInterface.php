<?php
namespace Keepper\Lib\Events\Interfaces;

interface EventSubscriberInterface {

	/**
	 * Добавляет слушателя указанного события
	 * @param string $eventName
	 * @param callable $listener
	 */
	public function addListener(string $eventName, callable $listener): void;

	/**
	 * Удаляет слушателя указанного события
	 * @param string $eventName
	 * @param callable $listener
	 */
	public function removeListener(string $eventName, callable $listener): void;

	/**
	 * Возвращает всех слушателей указанного события
	 * @param string $eventName
	 * @return EventListenerCollectionInterface
	 */
	public function getListeners(string $eventName): EventListenerCollectionInterface;

	/**
	 * Возвращает признак того, что у события есть слушатели
	 * @param string $eventName
	 * @return bool
	 */
	public function hasListeners(string $eventName): bool;
}