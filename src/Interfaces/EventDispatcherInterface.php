<?php
namespace Keepper\Lib\Events\Interfaces;

interface EventDispatcherInterface {

	/**
	 * Генерирует указанное событие
	 * @param string $eventName
	 * @param array $arguments
	 */
	public function dispatch(string $eventName, array $arguments = []): void;

	/**
	 * Возвращает объект через который можно подписаться на события данного Диспечер
	 * @return EventSubscriberInterface
	 */
	public function subscriber(): EventSubscriberInterface;
}