<?php
namespace Keepper\Lib\Events\Interfaces;

interface EventListenerCollectionInterface extends \Countable, \Iterator {
	/**
	 * Возвращает количество слушателей в коллекции
	 * @return int
	 */
	public function count(): int;

	public function current(): callable;

	/**
	 * Имя события на которые подписаны слушатели в данной коллекции
	 * @return string
	 */
	public function eventName(): string;
}