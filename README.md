# lib-events

Библиотека с реализацией концепции Системы событий основанной на следующих принципах:
1. Обработчики событий не должны влиять на выполенение основной бизнес логики, и являются второстепенным функционалом, нарушение работы которого вторично, и не должно нарушать работу основого процесса. 

    Поэтому все обработчики запускаются с погашеним Исключений и ошибок (по желанию ошибки в обработчиках могут логироваться)

1. Отсутствует понятие приоритетности или очереди запуска обработчиков. Если возникает потребность в приоритетности, значет в коде нарушена архитектура, и вы подписываетесь не на то событие. Пример:
    
    Есть событие поступления оплаты счетов. _Bill::newPayment_. Это событие из модуля **Счетов**, основная бизнес логика которого, обслуживает выставление счетов на внешние системы (Киви, Яндекс, Банк)и отслеживает их оплату.
     
    На данное событие подписан модуль **Баланса** пользователя, который по данному событию пополняет внутрениий баланс. Что является вторично логикой, и не должно влиять на логику модуля **Счетов**. _В случае сбоя, мы всегда можем произвести сверку БД, и восстановить данные баланса._ 
    
    Предположим, нам нужно высылать email при попоплнении баланса. Большинство разработчиков, подписывается на события поступления оплаты, и при этом им нужно гарантировать, что обработчик запуститься после Обработчика который записивает баланс.
    
    Это архитектурная ошибка. Рассмотрим ее. Во первых, как было показано ранее, логика пополнения баланса в обработчике события оплаты счета - вторична.
    Во вторых, отправка email, вторична по отношению к логике пополнения баланса. Отсюда уже получается некая иерархия:
    
        BillPayed -> BalanceAdded -> Send Email 
    
        Вместо
        
        BillPayed
         |
         -> BalanceAdded
         |
         -> Send Email
         
    Во втором случае нам нужно гарантировать порядок, в первом нет. Как реализовать первый случай? Очень просто модуль Баланса должен кидать свое событие, на которое уже и подписывается отправщие email. Тем самым мы уходим от приоритетов, и влиянии (Исключения) дочернего функционала на основной. Да еще получаем гибкость. Ведь если мы добавим иной способ пополнения баланса, скажем за участие в акции, у нас будет корректно работать отправка email, а при предыдущем подходе, мы бы добавляли копию обработчика на какоенибудь события подтверждения участия в акции.

1. Диспечером событий владеет, только код который предоставляет логику запуска своих событий. Никакой сторонний код не должен кидать чужие событий.

    Остановимся на данном случае подробнее. 
    
    Я думаю не нужно объяснять почему кидать чужие события зло (Модуль отправки email не должен иметь возможность кинуть событие подтверждения оплаты счета)
    
    Как этого добиться. Нужно, чтобы в публичном интерфейсе модуля, был доступен только объект через который можно подписаться на его события, и не должен быть доступен Диспечер событий. Также желательно, чтобы у каждого модуля был свой диспечер, а не общий на все приложение.
    
    
    
Вот на этих принципах построена данная библиотека.
    